# Project

### Every service must have
- [ ] Linter CI (gometalinter).
- [ ] build status & coverage badges in Readme.
- [ ] error code in error return.
- [ ] trace ID в точке входа.
- [ ] каждая функция должна отправлять trace информацию.
- [ ] grpc протоколы в отдельном репозитации.